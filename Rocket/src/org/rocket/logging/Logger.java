package org.rocket.logging;
/**
 * Represents server logger
 * @author PizzaCrust
 *
 */
public interface Logger {
	/**
	 * Logs to net.minecraft.server wrapped gui client
	 * @param message the message that will be logged
	 */
	void log(String message);
}
