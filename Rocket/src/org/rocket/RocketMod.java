package org.rocket;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;

import org.rocket.plugin.java.RocketPluginLoader;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;

/**
 * Core mod to load the mod implementation to the Server.
 * @author PizzaCrust
 *
 */
@Mod(modid = "Rocket", name = "Rocket Server", version = "1.0-BLEEDINGEDGE", acceptableRemoteVersions = "*")
public class RocketMod {
	ArrayList pluginsList;
	String workingDir = System.getProperty("user.dir");
	File parentFile = new File(workingDir);
	File pluginFolder = new File(workingDir, "plugins");
	@EventHandler
	public void setup(FMLServerStartingEvent e){
		if(!pluginFolder.exists()){
			pluginFolder.mkdir();
		}
		pluginsList = new ArrayList();
		RocketPluginLoader rocketPluginLoader = new RocketPluginLoader();
		File dir = pluginFolder;
		File [] files = dir.listFiles(new FilenameFilter() {
		    @Override
		    public boolean accept(File dir, String name) {
		        return name.endsWith(".jar");
		    }
		});

		for (File jarfile : files) {
		    try {
				rocketPluginLoader.func_01321(jarfile);
				pluginsList.add(RocketPluginLoader.NAME);
		    } catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}
}
