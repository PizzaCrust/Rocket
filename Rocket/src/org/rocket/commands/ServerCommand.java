package org.rocket.commands;
/**
 * Represents a base for creating commands.
 * @author PizzaCrust
 *
 */
public interface ServerCommand {
	/**
	 * The command name for the command.
	 * @return must return the command name of the command
	 */
	String commandName();
	/**
	 * The command usage for the command.
	 * @return must return the command usage of the command
	 */
	String commandUsage();
	/**
	 * When the command is executed, this method will run.
	 * @param sender the sender of the command
	 * @param args the arguments the sender has put
	 */
	void onExecute(CommandSender sender, String[] args);
}
