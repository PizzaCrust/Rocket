package org.rocket.commands;

/**
 * Representation of the command sender of a command.
 * @author PizzaCrust
 *
 */
public interface CommandSender {
	/**
	 * Sends a message to the CommandSender
	 * @param message that will be sent to the CommandSender
	 */
	void sendMessage(String message);
	/**
	 * Checks to see if the the sender is a player
	 * @return a boolean containing if the sender is a player
	 */
	boolean isPlayer();
	/**
	 * Checks to see if the sender is a Rcon
	 * @return a boolean containing if the sender is a rcon
	 */
	boolean isRcon();
}
