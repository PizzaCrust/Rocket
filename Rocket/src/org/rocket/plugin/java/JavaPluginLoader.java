package org.rocket.plugin.java;

import java.io.File;

/**
 * Handles all plugin loading. Do not use, use PluginManager!
 * @author PizzaCrust
 * @see PluginManager
 */
public interface JavaPluginLoader {
	/**
	 * Loads a plugin from the file
	 * @param file the file object that will be loaded
	 */
	boolean func_01321(File file) throws Exception;
}
