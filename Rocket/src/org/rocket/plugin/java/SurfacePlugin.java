package org.rocket.plugin.java;

import org.rocket.plugin.events.PluginInitializationEvent;

/**
 * Represents a SurfacePlugin.
 * @author PizzaCrust
 *
 */
public interface SurfacePlugin {
	/**
	 * Executes when plugin is enabled
	 * @param e the PluginInitializationEvent (should change on every implementation)
	 */
	void commenced(PluginInitializationEvent e);
	/**
	 * Executes after the plugin is disabled
	 */
	void conclude();
}
