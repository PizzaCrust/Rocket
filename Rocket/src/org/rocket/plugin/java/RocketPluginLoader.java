package org.rocket.plugin.java;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.rocket.plugin.events.PluginInitializationEvent;

import com.esotericsoftware.yamlbeans.YamlReader;
/**
 * Implements JavaPluginLoader.
 * @author PizzaCrust
 *
 */
public class RocketPluginLoader implements JavaPluginLoader{
	public RocketPluginLoader(){}
	public static String NAME;
	public static String VERSION;
	public static String MAIN_PATH;
	public static Class mainC;
	public boolean func_01321(File file) throws Exception {
		URL url = file.toURI().toURL();
		JarFile jarFile = new JarFile(file);
		URL[] urls = new URL[]{url};
		URLClassLoader classLoader = new URLClassLoader(urls);
		JarEntry pluginYAML = jarFile.getJarEntry("plugininfo.yml");
		InputStream inputStream = jarFile.getInputStream(pluginYAML);
		YamlReader yaml = new YamlReader(new InputStreamReader(inputStream));
		Object yamlCon = yaml.read();
		Map map = (Map) yamlCon;
		NAME = (String) map.get("name");
		VERSION = (String) map.get("version");
		MAIN_PATH = (String) map.get("main");
		Class mainClass = classLoader.loadClass(MAIN_PATH);
		Class[] noparams = new Class[]{ PluginInitializationEvent.class };
		Method method = mainClass.getDeclaredMethod("commenced", PluginInitializationEvent.class);
		method.setAccessible(true);
		method.invoke(mainClass, new org.rocket.plugin.events.PluginInitializationEvent());
		mainC = mainClass;
		return true;
	}

}
