package org.rocket.plugin.events;

import org.rocket.commands.ServerCommand;
import org.rocket.logging.Logger;
import org.rocket.plugin.java.SurfacePlugin;

/**
 * Represents a event that will allow management of plugin init.
 * @author PizzaCrust
 *
 */
public interface SurfaceInitializationEvent {
	/**
	 * Disables the plugin specified.
	 * @param surfacePlugin the plugin that will be disabled
	 */
	void disablePlugin(SurfacePlugin surfacePlugin);
	/**
	 * Finds if a plugin exists
	 * @param pluginName the plugin that will be checked
	 * @return returns a boolean if plugin exists
	 */
	boolean findPlugin(String pluginName);
	/**
	 * Registers server command
	 * @param serverCommand the command that will be registered
	 */
	void registerCommand(ServerCommand serverCommand);
	/**
	 * Gets the plugin logger
	 * @return the plugin logger
	 */
	Logger getLogger();
}
