package org.rocket.plugin;

import java.io.File;


/**
 * Represents a manager for plugins.
 * @author PizzaCrust
 *
 */
public interface PluginManager {
	/**
	 * Disables the plugin specified
	 * @param plugin the plugin that will be disabled
	 */
	void disablePlugin(org.rocket.plugin.java.SurfacePlugin plugin);
	/**
	 * Loads and enables a plugin from a file. (uses JavaPluginLoader)
	 * @param file the file that will be loaded
	 */
	void loadPlugin(File file);
}
