# Rocket
The **official** server implementation of Surface.  
Rocket is currently in active development and only has some of Surface implemented.  
This is not ready to be implemented inside of production servers and plugins may not  
work properly because of the new rewrite of JavaPluginLoader.   
   
**JAR files are not distributed from us, and will not until we finish a actual release.**